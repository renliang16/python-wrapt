Name:           python-wrapt
Version:        1.12.1
Release:        4
Summary:        A Python module for decorators, wrappers and monkey patching
License:        BSD-2-Clause
URL:            https://github.com/GrahamDumpleton/wrapt
Source0:        https://github.com/GrahamDumpleton/wrapt/archive/%{version}.tar.gz#/wrapt-%{version}.tar.gz

BuildRequires:  python3-devel python3-sphinx gcc

%description
The aim of the wrapt module is to provide a transparent object proxy for Python,
which can be used as the basis for the construction of function wrappers and decorator functions.
The wrapt module focuses very much on correctness. It therefore goes way beyond existing mechanisms
such as functools.wraps() to ensure that decorators preserve introspectability, signatures,
type checking abilities etc. The decorators that can be constructed using this module will work in
far more scenarios than typical decorators and provide more predictable and consistent behaviour.

%package help
Summary:        Documentation for the python-wrapt
BuildRequires:  python3-sphinx python3-sphinx_rtd_theme
Provides:       %{name}-doc = %{version}-%{release}
Obsoletes:      %{name}-doc < %{version}-%{release}

%description help
Documentation for the python-wrapt.

%package -n python3-wrapt
Summary:        Python3 module for wrapt module

%description -n python3-wrapt
Python3 module for wrapt module.

%prep
%autosetup -n wrapt-%{version} -p1

rm -rf wrapt.egg-info

%build
CFLAGS="$RPM_OPT_FLAGS" %{__python3} setup.py build

cd docs
sphinx-build -b html -d build/doctrees . build/html
cd -

%install
%{__python3} setup.py install --skip-build --root %{buildroot}

%files -n python3-wrapt
%doc README.rst LICENSE
%{python3_sitearch}/{wrapt,wrapt-%{version}-py*.egg-info}

%files help
%doc docs/build/html

%changelog
* Tue May 10 2022 yangping <yangping69@h-partners> - 1.12.1-4
- License compliance rectification

* Wed Mar 30 2022 xu_ping <xuping33@huawei.com> - 1.12.1-3
- Fix non-recognition ? the installed failed

* Mon May 31 2021 huanghaitao <huanghaitao8@huawei.com> - 1.12.1-2
- Completing build dependencies

* Wed Oct 14 2020 Zhipeng Xie <xiezhipeng1@huawei.com> - 1.12.1-1
- upgrade to 1.12.1

* Tue Aug 11 2020 zhangjiapeng <zhangjiapeng9@huawei.com> - 1.10.11-7
- Remove python2-wrapt subpackage

* Mon Feb 17 2020 daiqianwen <daiqianwen@huawei.com> - 1.10.11-6
- Package init
